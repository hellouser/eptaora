原创作者：**Ekaggrat Singh Kalsi**

原始仓库：[https://github.com/ekaggrat/eptaora](https://github.com/ekaggrat/eptaora)

油管地址：[https://www.youtube.com/watch?v=qoPwPJhESA8](https://www.youtube.com/watch?v=qoPwPJhESA8)

Hackaday：[https://hackaday.io/project/187684-eptaora](https://hackaday.io/project/187684-eptaora)

Thingiverse：[https://www.thingiverse.com/thing:5555533](https://www.thingiverse.com/thing:5555533)

声明：本仓库仅作为原 Github 仓库之备份，一切权益归原作者所有！

![eptaora](eptaora.jpg)
